Feature:  Verify the current location functionality
    
    As a user when I click on current location 
    then I can see my location on screen.

    Background:

        Given I open the url

    Scenario: Verify the location displayed on Ui vs actual.
        When I click on current location
        Then It should display you current city

   